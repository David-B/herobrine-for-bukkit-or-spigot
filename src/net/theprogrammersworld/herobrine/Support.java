package net.theprogrammersworld.herobrine;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;

import net.theprogrammersworld.herobrine.hooks.FactionsHook;
import net.theprogrammersworld.herobrine.hooks.GriefPreventionHook;
import net.theprogrammersworld.herobrine.hooks.PreciousStonesHook;
import net.theprogrammersworld.herobrine.hooks.RedProtectHook;
import net.theprogrammersworld.herobrine.hooks.ResidenceHook;
import net.theprogrammersworld.herobrine.hooks.TownyHook;
import net.theprogrammersworld.herobrine.hooks.WorldGuardHook;

public class Support {
	private boolean B_Residence;
	private boolean B_GriefPrevention;
	private boolean B_Towny;
	private boolean B_WorldGuard;
	private boolean B_CustomItems = false;
	private boolean B_PreciousStones;
	private boolean B_Factions;
	private boolean B_RedProtect;
	private ResidenceHook ResidenceCore;
	private GriefPreventionHook GriefPreventionCore;
	private TownyHook TownyCore;
	private WorldGuardHook WorldGuard;
	private PreciousStonesHook PreciousStones;
	private FactionsHook Factions;
	private RedProtectHook RedProtect;

	public Support() {
		ResidenceCore = new ResidenceHook();
		GriefPreventionCore = new GriefPreventionHook();
		TownyCore = new TownyHook();
		WorldGuard = new WorldGuardHook();
		PreciousStones = new PreciousStonesHook();
		Factions = new FactionsHook();
		RedProtect = new RedProtectHook();
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Herobrine.getPluginCore(), new Runnable() {
			@Override
			public void run() {
				Support.this.CheckForPlugins();
			}
		}, 2L);
	}

	public boolean isPreciousStones() {
		return B_PreciousStones;
	}

	public boolean isWorldGuard() {
		return B_WorldGuard;
	}

	public boolean isResidence() {
		return B_Residence;
	}

	public boolean isGriefPrevention() {
		return B_GriefPrevention;
	}

	public boolean isTowny() {
		return B_Towny;
	}

	public boolean isFactions() {
		return B_Factions;
	}

	public boolean isRedProtect() {
		return B_RedProtect;
	}

	public void CheckForPlugins() {
		if (ResidenceCore.Check()) {
			B_Residence = true;
			Herobrine.log.info("[Herobrine] Residence plugin detected on server");
		}
		if (GriefPreventionCore.Check()) {
			B_GriefPrevention = true;
			Herobrine.log.info("[Herobrine] GriefPrevention plugin detected on server");
		}
		if (TownyCore.Check()) {
			B_Towny = true;
			Herobrine.log.info("[Herobrine] Towny plugin detected on server");
		}
		if (WorldGuard.Check()) {
			B_WorldGuard = true;
			Herobrine.log.info("[Herobrine] WorldGuard plugin detected on server");
		}
		if (PreciousStones.Check()) {
			B_PreciousStones = true;
			Herobrine.log.info("[Herobrine] PreciousStones plugin detected on server");
		}
		if (Factions.Check()) {
			B_Factions = true;
			Herobrine.log.info("[Herobrine] Factions plugin detected on server");
		}
		if (RedProtect.Check()) {
			B_RedProtect = true;
			Herobrine.log.info("[Herobrine] RedProtect plugin detected on server");
		}
	}

	public boolean isSecuredArea(final Location loc) {
		if (B_Residence && ResidenceCore.isSecuredArea(loc)) {
			return true;
		}
		if (B_GriefPrevention && GriefPreventionCore.isSecuredArea(loc)) {
			return true;
		}
		if (B_Towny && TownyCore.isSecuredArea(loc)) {
			return true;
		}
		if (B_WorldGuard && WorldGuard.isSecuredArea(loc)) {
			return true;
		}
		if (B_PreciousStones && PreciousStones.isSecuredArea(loc)) {
			return true;
		}
		if (B_Factions && Factions.isSecuredArea(loc)) {
			return true;
		}
		if (B_RedProtect && RedProtect.isSecuredArea(loc)) {
			return true;
		}
		return false;
	}

	public boolean checkBuild(final Location loc) {
		if (Herobrine.getPluginCore().getConfigDB().SecuredArea_Build || !isSecuredArea(loc)) {
			return true;
		}
		return false;
	}

	public boolean checkAttack(final Location loc) {
		return Herobrine.getPluginCore().getConfigDB().SecuredArea_Attack || !isSecuredArea(loc);
	}

	public boolean checkHaunt(final Location loc) {
		return Herobrine.getPluginCore().getConfigDB().SecuredArea_Haunt || !isSecuredArea(loc);
	}

	public boolean checkSigns(final Location loc) {
		if (Herobrine.getPluginCore().getConfigDB().SecuredArea_Signs || !isSecuredArea(loc)) {
			if (loc.getBlock().getType().equals(Material.AIR)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public boolean checkBooks(final Location loc) {
		return Herobrine.getPluginCore().getConfigDB().SecuredArea_Books || !isSecuredArea(loc);
	}

	public boolean isCustomItems() {
		return this.B_CustomItems;
	}
}