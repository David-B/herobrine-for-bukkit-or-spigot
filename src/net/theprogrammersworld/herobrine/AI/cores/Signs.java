package net.theprogrammersworld.herobrine.AI.cores;

import java.util.Random;

import net.theprogrammersworld.herobrine.Herobrine;
import net.theprogrammersworld.herobrine.AI.AICore;
import net.theprogrammersworld.herobrine.AI.Core;
import net.theprogrammersworld.herobrine.AI.CoreResult;
import net.theprogrammersworld.herobrine.misc.BlockChanger;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Directional;

public class Signs extends Core {

	public Signs() {
		super(CoreType.SIGNS, AppearType.NORMAL);
	}

	@Override
	public CoreResult callCore(final Object[] data) {
		return placeSign((Location) data[0], (Location) data[1]);
	}

	public CoreResult placeSign(final Location loc, final Location ploc) {
		boolean status = false;
		AICore.log.info("Generating sign...");
		if ((loc.getWorld().getBlockAt(loc.getBlockX() + 2, loc.getBlockY(), loc.getBlockZ()).getType() == Material.AIR)
				&& (loc.getWorld().getBlockAt(loc.getBlockX() + 2, loc.getBlockY() - 1, loc.getBlockZ()).getType() != Material.AIR)) {
			loc.setX(loc.getBlockX() + 2);
			createSign(loc, ploc);
			status = true;
		} else if ((loc.getWorld().getBlockAt(loc.getBlockX() - 2, loc.getBlockY(), loc.getBlockZ()).getType() == Material.AIR)
				&& (loc.getWorld().getBlockAt(loc.getBlockX() - 2, loc.getBlockY() - 1, loc.getBlockZ()).getType() != Material.AIR)) {
			loc.setX(loc.getBlockX() - 2);
			createSign(loc, ploc);
			status = true;
		} else if ((loc.getWorld().getBlockAt(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ() + 2).getType() == Material.AIR)
				&& (loc.getWorld().getBlockAt(loc.getBlockX(), loc.getBlockY() - 1, loc.getBlockZ() + 2).getType() != Material.AIR)) {
			loc.setZ(loc.getBlockZ() + 2);
			createSign(loc, ploc);
			status = true;
		} else if ((loc.getWorld().getBlockAt(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ() - 2).getType() == Material.AIR)
				&& (loc.getWorld().getBlockAt(loc.getBlockX(), loc.getBlockY() - 1, loc.getBlockZ() - 2).getType() != Material.AIR)) {
			loc.setZ(loc.getBlockZ() - 2);
			createSign(loc, ploc);
			status = true;
		}
		if (status) {
			return new CoreResult(true, "Sign generated successfully.");
		}
		return new CoreResult(false, "Sign generation failed.");
	}

	public void createSign(final Location loc, final Location ploc) {
		final Random randcgen = new Random();
		final int chance = randcgen.nextInt(100);
		if (chance > (100 - Herobrine.getPluginCore().getConfigDB().SignChance)) {
			final Random randgen = new Random();
			final int count = Herobrine.getPluginCore().getConfigDB().useSignMessages.size();
			final int randmsg = randgen.nextInt(count);
			final Block signblock = loc.add(0.0, 0.0, 0.0).getBlock();
			final Block undersignblock = signblock.getLocation().subtract(0.0, 1.0, 0.0).getBlock();
			if (Herobrine.isAllowedBlock(signblock.getType()) && !Herobrine.isAllowedBlock(undersignblock.getType())) {
				signblock.setType(Material.matchMaterial("SIGN_POST"));
				
				final Sign sign = (Sign) signblock.getState();

				BlockData blockData = sign.getBlockData();
				((Directional) blockData).setFacing(BlockChanger.getPlayerBlockFace(ploc));
				sign.setBlockData(blockData);

				sign.setLine(1, Herobrine.getPluginCore().getConfigDB().useSignMessages.get(randmsg));

				sign.update();
			}
		}
	}

}