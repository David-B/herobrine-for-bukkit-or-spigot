package net.theprogrammersworld.herobrine.AI.cores;

import java.util.Random;

import net.theprogrammersworld.herobrine.Herobrine;
import net.theprogrammersworld.herobrine.AI.Core;
import net.theprogrammersworld.herobrine.AI.CoreResult;

import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class SoundF extends Core {

	public SoundF() {
		super(CoreType.SOUNDF, AppearType.NORMAL);
	}

	@Override
	public CoreResult callCore(final Object[] data) {
		return playRandom((Player) data[0]);
	}

	public CoreResult playRandom(final Player player) {
		if (!Herobrine.getPluginCore().getConfigDB().UseSound) {
			return new CoreResult(false, "Herobrine-caused sound is disabled on this server.");
		}
		final Sound[] sounds = { Sound.BLOCK_STONE_STEP, Sound.BLOCK_WOOD_STEP, Sound.BLOCK_GRASS_STEP, Sound.BLOCK_SAND_STEP,
				Sound.BLOCK_GRAVEL_STEP, Sound.ENTITY_WITHER_HURT, Sound.ENTITY_WITHER_HURT, Sound.ENTITY_WITHER_HURT,
				Sound.ENTITY_WITHER_HURT, Sound.BLOCK_WOODEN_DOOR_OPEN, Sound.BLOCK_WOODEN_DOOR_CLOSE, Sound.ENTITY_GHAST_SCREAM,
				Sound.ENTITY_GHAST_SCREAM, Sound.ENTITY_WITHER_DEATH, Sound.ENTITY_WITHER_HURT };
		final int chance = new Random().nextInt(14);
		int randx = new Random().nextInt(3);
		int randz = new Random().nextInt(3);
		final int randxp = new Random().nextInt(1);
		final int randzp = new Random().nextInt(1);
		if ((randxp == 0) && (randx != 0)) {
			randx = -randx;
		}
		if ((randzp == 0) && (randz != 0)) {
			randz = -randz;
		}
		player.playSound(player.getLocation(), sounds[chance], 0.75f, 0.75f);
		return new CoreResult(true, "SoundF is starting.");
	}

}