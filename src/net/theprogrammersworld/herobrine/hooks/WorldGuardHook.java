package net.theprogrammersworld.herobrine.hooks;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.regions.RegionContainer;
import com.sk89q.worldguard.protection.regions.RegionQuery;

public class WorldGuardHook {

	public boolean Check() {
		return Bukkit.getServer().getPluginManager().getPlugin("WorldGuard") != null;
	}

	public boolean isSecuredArea(final Location loc) {
		RegionContainer container = WorldGuard.getInstance().getPlatform().getRegionContainer();
		RegionQuery query = container.createQuery();
		ApplicableRegionSet set = query.getApplicableRegions(new com.sk89q.worldedit.util.Location(null, loc.getX(), loc.getY(), loc.getZ()));
		if (set != null) {
			return set.size() != 0;
		}
		else
		{
			return false;
		}
	}

}