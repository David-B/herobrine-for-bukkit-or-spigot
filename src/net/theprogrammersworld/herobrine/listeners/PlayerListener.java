package net.theprogrammersworld.herobrine.listeners;

import java.util.ArrayList;
import java.util.Random;

import net.theprogrammersworld.herobrine.Herobrine;
import net.theprogrammersworld.herobrine.AI.AICore;
import net.theprogrammersworld.herobrine.AI.Core;
import net.theprogrammersworld.herobrine.misc.ItemName;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Jukebox;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.ItemStack;

public class PlayerListener implements Listener {

	  private ArrayList<String> equalsLoreS = new ArrayList<String>();
	  private ArrayList<String> equalsLoreA = new ArrayList<String>();
	  private ArrayList<LivingEntity> LivingEntities = new ArrayList<LivingEntity>();
	  private Location le_loc = null;
	  private Location p_loc = null;
	  private long timestamp = 0L;
	  private boolean canUse = false;
	  
	  public PlayerListener() {
	    this.equalsLoreS.add("Herobrine�s artifact");
	    this.equalsLoreS.add("Sword of Lighting");
	    this.equalsLoreA.add("Herobrine�s artifact");
	    this.equalsLoreA.add("Apple of Death");
	  }
	  
	@EventHandler
	public void onPlayerInteract(final PlayerInteractEvent event) {
		if (((event.getAction() == Action.LEFT_CLICK_BLOCK) || (event.getAction() == Action.RIGHT_CLICK_BLOCK)) && 
			      (event.getClickedBlock() != null) && (event.getPlayer().getInventory().getItemInMainHand() != null))
			    {
			      ItemStack itemInHand = event.getPlayer().getInventory().getItemInMainHand();
			      if ((event.getPlayer().getInventory().getItemInMainHand().getType() != null) && 
			        ((itemInHand.getType() == Material.DIAMOND_SWORD) || (itemInHand.getType() == Material.GOLDEN_APPLE)) && 
			        (ItemName.getLore(itemInHand) != null)) {
			        if ((ItemName.getLore(itemInHand).containsAll(this.equalsLoreS)) && (Herobrine.getPluginCore().getConfigDB().UseArtifactSword))
			        {
			          if (new Random().nextBoolean()) {
			            event.getPlayer().getLocation().getWorld().strikeLightning(event.getClickedBlock().getLocation());
			          }
			        }
			        else if ((ItemName.getLore(itemInHand).containsAll(this.equalsLoreA)) && (Herobrine.getPluginCore().getConfigDB().UseArtifactApple))
			        {
			          this.timestamp = (System.currentTimeMillis() / 1000L);
			          this.canUse = false;
			          if (Herobrine.getPluginCore().PlayerApple.containsKey(event.getPlayer()))
			          {
			            if (((Long)Herobrine.getPluginCore().PlayerApple.get(event.getPlayer())).longValue() < this.timestamp)
			            {
			              Herobrine.getPluginCore().PlayerApple.remove(event.getPlayer());
			              this.canUse = true;
			            }
			            else
			            {
			              this.canUse = false;
			            }
			          }
			          else {
			            this.canUse = true;
			          }
			          if (this.canUse)
			          {
			            event.getPlayer().getWorld().createExplosion(event.getPlayer().getLocation(), 0.0F);
			            this.LivingEntities = ((ArrayList<LivingEntity>)event.getPlayer().getLocation().getWorld().getLivingEntities());
			            Herobrine.getPluginCore().PlayerApple.put(event.getPlayer(), Long.valueOf(this.timestamp + 60L));
			            for (int i = 0; i <= this.LivingEntities.size() - 1; i++) {
			              if ((!(this.LivingEntities.get(i) instanceof Player)) && (this.LivingEntities.get(i).getEntityId() != Herobrine.herobrineEntityID))
			              {
			                this.le_loc = this.LivingEntities.get(i).getLocation();
			                this.p_loc = event.getPlayer().getLocation();
			                if ((this.le_loc.getBlockX() < this.p_loc.getBlockX() + 20) && (this.le_loc.getBlockX() > this.p_loc.getBlockX() - 20) && 
			                  (this.le_loc.getBlockY() < this.p_loc.getBlockY() + 10) && (this.le_loc.getBlockY() > this.p_loc.getBlockY() - 10) && 
			                  (this.le_loc.getBlockZ() < this.p_loc.getBlockZ() + 20) && (this.le_loc.getBlockZ() > this.p_loc.getBlockZ() - 20))
			                {
			                  event.getPlayer().getWorld().createExplosion(this.LivingEntities.get(i).getLocation(), 0.0F);
			                  this.LivingEntities.get(i).damage(10000);
			                }
			              }
			            }
			          }
			          else
			          {
			            event.getPlayer().sendMessage(ChatColor.RED + "Apple of Death is recharging!");
			          }
			        }
			      }
			    }
		
		
		if ((event.getClickedBlock() != null) && (event.getPlayer().getInventory().getItemInMainHand() != null) && (event.getClickedBlock().getType() == Material.JUKEBOX)) {
			final ItemStack item = event.getPlayer().getInventory().getItemInMainHand();
			final Jukebox block = (Jukebox) event.getClickedBlock().getState();
			if (!block.isPlaying() && (item.getType() == Material.matchMaterial("RECORD_11"))) {
				Herobrine.getPluginCore().getAICore();
				if (!AICore.isDiscCalled) {
					final Player player = event.getPlayer();
					Herobrine.getPluginCore().getAICore();
					AICore.isDiscCalled = true;
					Herobrine.getPluginCore().getAICore().cancelTarget(Core.CoreType.ANY, true);
					Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(AICore.plugin, new Runnable() {
						@Override
						public void run() {
							Herobrine.getPluginCore().getAICore().callByDisc(player);
						}
					}, 50L);
				}
			}
		}
	}

	@EventHandler
	public void onPlayerEnterBed(final PlayerBedEnterEvent event) {
		if (new Random().nextInt(100) > 75) {
			final Player player = event.getPlayer();
			event.setCancelled(true);
			Herobrine.getPluginCore().getAICore().playerBedEnter(player);
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerQuit(final PlayerQuitEvent event) {
		if (event.getPlayer().getEntityId() != Herobrine.herobrineEntityID) {
			Herobrine.getPluginCore().getAICore();
			if ((AICore.PlayerTarget == event.getPlayer()) && (Herobrine.getPluginCore().getAICore().getCoreTypeNow() == Core.CoreType.GRAVEYARD) && (event.getPlayer().getLocation().getWorld() == Bukkit.getServer().getWorld(Herobrine.getPluginCore().getConfigDB().HerobrineWorldName))) {
				Herobrine.getPluginCore().getAICore();
				if (AICore.isTarget) {
					event.getPlayer().teleport(Herobrine.getPluginCore().getAICore().getGraveyard().getSavedLocation());
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerKick(final PlayerKickEvent event) {
		if (event.getPlayer().getEntityId() == Herobrine.herobrineEntityID) {
			event.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerTeleport(final PlayerTeleportEvent event) {
		if (event.getPlayer().getEntityId() == Herobrine.herobrineEntityID) {
	      if (event.getFrom().getWorld() != event.getTo().getWorld()) {
	        Herobrine.getPluginCore().removeHerobrine();
	        Herobrine.getPluginCore().spawnHerobrine(event.getTo());
	        event.setCancelled(true);
	        return;
	      }
	      if ((Herobrine.getPluginCore().getAICore().getCoreTypeNow() == Core.CoreType.RANDOM_POSITION) && 
	        (Herobrine.herobrineNPC.getNMSEntity().getBukkitEntity().getLocation().getBlockX() > Herobrine.getPluginCore().getConfigDB().WalkingModeXRadius) && 
	        (Herobrine.herobrineNPC.getNMSEntity().getBukkitEntity().getLocation().getBlockX() < -Herobrine.getPluginCore().getConfigDB().WalkingModeXRadius) && 
	        (Herobrine.herobrineNPC.getNMSEntity().getBukkitEntity().getLocation().getBlockZ() > Herobrine.getPluginCore().getConfigDB().WalkingModeZRadius) && 
	        (Herobrine.herobrineNPC.getNMSEntity().getBukkitEntity().getLocation().getBlockZ() < -Herobrine.getPluginCore().getConfigDB().WalkingModeZRadius)) {
	        Herobrine.getPluginCore().getAICore().cancelTarget(Core.CoreType.RANDOM_POSITION, true);
	        Herobrine.herobrineNPC.moveTo(new Location(Bukkit.getServer().getWorlds().get(0), 0.0D, -20.0D, 0.0D));
	      }
	    } 
	}
			
	@EventHandler
	public void onPlayerCommand(final PlayerCommandPreprocessEvent event) {
		if (event.getPlayer().getWorld() == Bukkit.getServer().getWorld(Herobrine.getPluginCore().getConfigDB().HerobrineWorldName) && !event.getPlayer().hasPermission("hb-ai.cmdblockbypass")) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		// If the plugin is configured to show Herobrine in the tab list, add him to the list
		// when the user logs on.
		if(Herobrine.getPluginCore().getConfigDB().ShowInTabList) {
			AICore.showHero(event.getPlayer());
		}
	}

	@EventHandler
	public void onPlayerDeathEvent(final PlayerDeathEvent event) {
		if (event.getEntity().getEntityId() == Herobrine.herobrineEntityID) {
			event.setDeathMessage("");
			Herobrine.getPluginCore().removeHerobrine();
			final Location nowloc = new Location(Bukkit.getServer().getWorlds().get(0), 0.0, -20.0, 0.0);
			nowloc.setYaw(1.0f);
			nowloc.setPitch(1.0f);
			Herobrine.getPluginCore().spawnHerobrine(nowloc);
		}
	}

	@EventHandler
	public void onPlayerMoveEvent(final PlayerMoveEvent event) {
		if ((event.getPlayer().getEntityId() != Herobrine.herobrineEntityID) && (event.getPlayer().getWorld() == Bukkit.getServer().getWorld(Herobrine.getPluginCore().getConfigDB().HerobrineWorldName))) {
			final Player player = event.getPlayer();
			player.teleport(new Location(Bukkit.getServer().getWorld(Herobrine.getPluginCore().getConfigDB().HerobrineWorldName), -2.49, 4.0, 10.69, -179.85f, 0.44999f));
		}
	}
	
}