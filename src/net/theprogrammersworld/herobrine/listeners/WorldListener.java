package net.theprogrammersworld.herobrine.listeners;

import java.util.Random;

import net.theprogrammersworld.herobrine.Herobrine;
import net.theprogrammersworld.herobrine.AI.Core;

import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkLoadEvent;

public class WorldListener implements Listener {

	@EventHandler
	public void onChunkLoad(final ChunkLoadEvent event) {
		if (event.isNewChunk()) {
			final World world = event.getWorld();
			
			if (Herobrine.getPluginCore().getConfigDB().useWorlds.contains(world.getName())) {
				int pyramidChance = Herobrine.getPluginCore().getConfigDB().BuildPyramidOnChunkPercentage;
				int templeChance = Herobrine.getPluginCore().getConfigDB().BuildTempleOnChunkPercentage;
				
				if (Herobrine.getPluginCore().getConfigDB().BuildPyramids && (new Random().nextInt(100) + 1 <= pyramidChance)) {
					final Object[] data = { event.getChunk() };
					Herobrine.getPluginCore().getAICore().getCore(Core.CoreType.PYRAMID).runCore(data);
				}
				if (Herobrine.getPluginCore().getConfigDB().BuildTemples && (new Random().nextInt(100) + 1 <= templeChance)) {
					final Object[] data = { event.getChunk() };			            
		            Herobrine.getPluginCore().getAICore().getCore(Core.CoreType.TEMPLE).runCore(data);	
				}
			}
		}
	}

}