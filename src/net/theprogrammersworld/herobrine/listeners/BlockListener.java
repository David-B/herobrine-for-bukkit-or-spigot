package net.theprogrammersworld.herobrine.listeners;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import net.theprogrammersworld.herobrine.Herobrine;
import net.theprogrammersworld.herobrine.AI.Core;
import net.theprogrammersworld.herobrine.AI.cores.Heads;

public class BlockListener implements Listener {

	@EventHandler
	public void onBlockIgnite(final BlockIgniteEvent event) {
		if (event.getBlock().getWorld() == Bukkit.getServer()
				.getWorld(Herobrine.getPluginCore().getConfigDB().HerobrineWorldName)) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onBlockBreak(final BlockBreakEvent event) {
		if (event.getBlock().getWorld() == Bukkit.getServer()
				.getWorld(Herobrine.getPluginCore().getConfigDB().HerobrineWorldName)) {
			event.setCancelled(true);
			return;
		}
		final Heads h = (Heads) Herobrine.getPluginCore().getAICore().getCore(Core.CoreType.HEADS);
		final ArrayList<Block> list = h.getHeadList();
		if (list.contains(event.getBlock())) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onBlockPlace(final BlockPlaceEvent event) {
		if (event.getBlock().getWorld() == Bukkit.getServer()
				.getWorld(Herobrine.getPluginCore().getConfigDB().HerobrineWorldName)) {
			event.setCancelled(true);
		}
	}

}