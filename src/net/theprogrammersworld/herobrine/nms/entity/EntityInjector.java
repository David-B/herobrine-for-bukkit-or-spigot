package net.theprogrammersworld.herobrine.nms.entity;

import net.minecraft.server.v1_14_R1.Entity;
import net.minecraft.server.v1_14_R1.EntityTypes;
import net.minecraft.server.v1_14_R1.EnumCreatureType;
import net.minecraft.server.v1_14_R1.IRegistry;
import net.theprogrammersworld.herobrine.Herobrine;

public class EntityInjector {

	public static void inject() {
		try {			
			addCustomEntity("mskeleton", CustomSkeleton::new, EnumCreatureType.MONSTER);
			addCustomEntity("mzombie", CustomZombie::new, EnumCreatureType.MONSTER);
		} catch (Throwable t) {
			t.printStackTrace();
			Herobrine.isNPCDisabled = true;
			Herobrine.log.warning("[Herobrine] Custom NPCs have been disabled due to a compatibility issue.");
		}
	}

	// https://www.spigotmc.org/threads/handling-custom-entity-registry-on-spigot-1-13.353426/#post-3447111
	private static <T extends Entity> void addCustomEntity(String customName, EntityTypes.b<T> _func, EnumCreatureType enumCreatureType) {
		EntityTypes.a<?> entity = EntityTypes.a.a(_func, enumCreatureType);
		entity.b();
		IRegistry.a(IRegistry.ENTITY_TYPE, customName, entity.a(customName));
	}
}
